window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/";

    try {
        const response = await fetch(url);
        if (!(response.ok)) {
            console.error("Invalid response from api/conferences!!");
        } else {
            const data = await response.json();
            const selectTag = document.getElementById("conference");
            for (let conference of data.conferences) {
                const option = document.createElement("option");
                option.value = conference.pk;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }
        }
    } catch(e) {
        console.error("Error!! ", e)
    }

    const formTag = document.getElementById("create-presentation-form");
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const formObj = Object.fromEntries(formData);
        const presentationUrl = `http://localhost:8000/api/conferences/${formObj.conference}/presentations/`;
        delete formObj.conference;
        const json = JSON.stringify(formObj);

        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
        }
    });
});
