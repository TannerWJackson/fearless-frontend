function createCard(title, description, pictureUrl, date, locationName) {
    return `
      <div class="card shadow mb-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${date}
        </div>
      </div>
    `;
  }

  function createPlaceholder(phCounter) {
    return `
        <div class="card" aria-hidden="true" id=${phCounter}>
            <img src="" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title placeholder-glow">
                    <span class="placeholder col-6"></span>
                </h5>
            <p class="card-text placeholder-glow">
                <span class="placeholder col-7"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-6"></span>
                <span class="placeholder col-8"></span>
            </p>
            <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
            </div>
        </div>`
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const row = document.querySelector(".row");
        row.innerHTML += '<div class="alert alert-danger" role="alert">Invalid response from api/conferences!</div>'
      } else {
        const data = await response.json();
        let colCounter = 1;
        let phCounter = 0;
        for (let conference of data.conferences) {

            if (colCounter < 3) {
                phCounter++;
                const phHtml = createPlaceholder(phCounter);
                const column = document.getElementById(`col-${colCounter}`);
                column.innerHTML += phHtml;
                colCounter++;
            } else if (colCounter === 3) {
                phCounter++;
                const phHtml = createPlaceholder(phCounter);
                const column = document.getElementById("col-3");
                column.innerHTML += phHtml;
                colCounter = 1;
            }

            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {

                const placeholder = document.getElementById(`${phCounter}`);

                const details = await detailResponse.json();

                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDateObj = new Date(Date.parse(details.conference.starts));
                const endDateObj = new Date(Date.parse(details.conference.ends));
                const fullDate = `${startDateObj.getMonth()}/${startDateObj.getDate()}/${startDateObj.getFullYear()} - ${endDateObj.getMonth()}/${endDateObj.getDate()}/${endDateObj.getFullYear()}`;
                const locationName = details.conference.location.name;

                const html = createCard(title, description, pictureUrl, fullDate, locationName);

                if (colCounter < 3) {
                    const column = document.getElementById(`${placeholder.parentNode.id}`);
                    if (placeholder.parentNode) {
                        placeholder.parentNode.removeChild(placeholder);
                    }
                    column.innerHTML += html;
                } else if (colCounter === 3) {
                    const column = document.getElementById(`${placeholder.parentNode.id}`);
                    if (placeholder.parentNode) {
                        placeholder.parentNode.removeChild(placeholder);
                    }
                    column.innerHTML += html;
                }
            }
        }
    }
    } catch (e) {
      const row = document.querySelector(".row");
      row.innerHTML += `<div class="alert alert-danger" role="alert">Error! ${e}</div>`
    }

  });
